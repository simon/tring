#!/bin/sh

# Change pwd to where this script lives.
cd "$(dirname "$(readlink -f "$0")")"

# We might be running from a bob delivery dir, with the 'tring' binary
# directly alongside us, or from a source dir with it in the build
# subdirectory. Find it.
for tring in tring build/tring ""; do
    if test -f "$tring"; then
        break # found it
    elif test -z "$tring"; then
        echo >&2 "Unable to find 'tring'"
        exit 1
    fi
done

if test $# -gt 0; then
  # Given a hostname on the command line, remotely install to its
  # /mnt/usb. Saves unplugging the USB key!
  rm -f tringtmp
  lns "$tring" tringtmp
  scp tringtmp debugchumby sshkeys excurl timeip "$1":/mnt/usb
  ssh "$1" mv /mnt/usb/tringtmp /mnt/usb/tring
  rm -f tringtmp
  ssh "$1" sync
else
  if mount -t vfat /dev/sdc1 /mnt; then
    cp build/tring /mnt
    cp debugchumby /mnt
    cp sshkeys /mnt
    cp excurl /mnt
    cp timeip /mnt
    umount /mnt
    sync
    sleep 2
    sync
    echo done
  fi
fi
