#!/usr/bin/env python3

# Python code to output SVGs for the various graphical elements of the
# Tring display.
#
# Usually the Tring makefile will convert these to bitmaps, but you
# can also try one out locally by running a command like
#
#   inkscape -b black -d 72 -e test.png image001.svg

import sys
import string
import argparse
import html

import xml.etree.cElementTree as ET

import cairo
import gi
gi.require_version("Pango", "1.0")
gi.require_version("PangoCairo", "1.0")
from gi.repository import Pango
from gi.repository import PangoCairo

# Default configuration. Note that these apparently global variables
# are not actually in the main program's globals - we run this
# function inside a special dict.
def defaultconfig():
    global buttonfont, messagefont, offdayfont, timefont
    global secratio
    global messagecolour, errormessagecolour, timecolour
    global buttonoutlinecolour, buttonactiveoutlinecolour
    global buttonbgcolour, buttonactivebgcolour
    global buttontextcolour, buttonactivetextcolour
    global offdayactiveoutlinecolour
    global offdayactivebgcolour
    global offdayactivetextcolour

    buttonfont = {"family":"Nimbus Sans L", "size":16, "bold":False}
    messagefont = {"family":"Nimbus Sans L", "size":24, "bold":True}
    offdayfont = {"family":"Nimbus Sans L", "size":24, "bold":False}
    timefont = {"family":"Nimbus Sans L", "bold":True} # size is automatic
    secratio = 0.6 # size of the seconds field relative to to HH:MM

    messagecolour = "#ff0000"
    errormessagecolour = "#ffffff"
    timecolour = "#ff0000"
    buttonoutlinecolour = "#ff0000"
    buttonactiveoutlinecolour = "#ff0000"
    buttonbgcolour = "#800000"
    buttonactivebgcolour = "#ff0000"
    buttontextcolour = "#ffffff"
    buttonactivetextcolour = "#ffffff"
    offdayactiveoutlinecolour = "#660000"
    offdayactivebgcolour = "#330000"
    offdayactivetextcolour = "#ff0000"

class FontMetricsFinder:
    weights = {False:"normal", True:"bold"}
    def __init__(self):
        self.surface = cairo.SVGSurface(None, 500, 500)
        self.cr = cairo.Context(self.surface)
        self.pcr = PangoCairo.create_context(self.cr)

    def __call__(self, text, font, size):
        layout = PangoCairo.create_layout(self.cr)

        layout.set_markup(
            '<span font_family="{}" size="{:d}" weight="{}">{}</span>'
            .format(html.escape(font["family"], '"'),
                    int(1000 * Pango.SCALE),
                    self.weights[font["bold"]],
                    html.escape(text)))

        exts = layout.get_line(0).get_extents()
        # I'm not completely sure I understand this magical scaling
        # factor of 0.75 that seems to be needed to make the extents
        # come out right, but I _think_ it has something to do with
        # SVG's default coordinate system not matching up to points.
        # Pages like http://stackoverflow.com/questions/1346922/
        # suggest that SVG doesn't even _have_ a well-defined default
        # coordinate system, i.e. the interpretation of bare
        # coordinate numbers (not qualified by 'in','mm',etc) is up to
        # the SVG viewer - in which case this scale value is actually
        # compensating for whatever value is baked into Cairo, not for
        # any kind of objective reality. However.
        scale = 0.75/(1000*Pango.SCALE)
        exts = [[u*size*scale for u in [t.x, t.y, t.width, t.height]]
                for t in exts]

        return exts

class Font(object):
    fontmetricsfinder = FontMetricsFinder()
    def __init__(self, font, override_size=None):
        self.font = font
        self.size = font["size"] if override_size is None else override_size
    def stringwidth(self, text):
        exts = self.fontmetricsfinder(text, self.font, self.size)
        return exts[1][2] # logical width
    def capsheight(self):
        exts = self.fontmetricsfinder(string.ascii_uppercase,
                                      self.font, self.size)
        return -exts[0][1] # actual ink ascent
    def digitheight(self):
        exts = self.fontmetricsfinder(string.digits, self.font, self.size)
        return -exts[0][1] # actual ink ascent
    def draw(self, group, x, y, text, attrs):
        t = ET.SubElement(group, "text")
        t.attrib["x"] = svgdim(x)
        t.attrib["y"] = svgdim(y)
        attrs = attrs.copy()
        attrs["font-size"] = svgdim(self.size)
        attrs["font-family"] = self.font["family"]
        if self.font["bold"]:
            attrs["font-weight"] = "bold"
        t.attrib["style"] = "; ".join([k+":"+v for k,v in attrs.items()])
        t.text = text

def setup_config(args):
    cfg = {}
    exec(defaultconfig.__code__, cfg)
    if args.config is not None:
        with open(args.config) as f:
            exec(f.read(), cfg)
    return cfg

def svgdim(n):
    return "%.2f" % n

def svgrect(group, x, y, w, h, attrs):
    rect = ET.SubElement(group, "rect")
    rect.attrib["x"] = svgdim(x)
    rect.attrib["y"] = svgdim(y)
    rect.attrib["width"] = svgdim(w)
    rect.attrib["height"] = svgdim(h)
    rect.attrib["style"] = "; ".join([k+":"+v for k,v in attrs.items()])

def svgpoly(group, coords, attrs):
    rect = ET.SubElement(group, "polygon")
    rect.attrib["points"] = " ".join(["%s,%s" % (svgdim(x),svgdim(y))
                                      for x,y in coords])
    rect.attrib["style"] = "; ".join([k+":"+v for k,v in attrs.items()])

class SVG(object):
    def __init__(self, w, h):
        self.root = ET.Element("svg")
        self.root.attrib["xmlns"] = "http://www.w3.org/2000/svg"
        self.root.attrib["width"] = svgdim(w) + "pt"
        self.root.attrib["height"] = svgdim(h) + "pt"
        # Ensure unqualified coordinates match up to points.
        self.root.attrib["viewBox"] = "0 0 %.2f %.2f" % (w,h)
    def group(self):
        return ET.SubElement(self.root, "g")
    def save(self, outname):
        ET.ElementTree(self.root).write(outname)

class Button(object):
    def __init__(self, cfg, text, xalign, yalign, kind, state):
        if kind == "normal":
            self.font = Font(cfg["buttonfont"])
        elif kind == "offday":
            self.font = Font(cfg["offdayfont"])
        else:
            assert False, "bad kind " + repr(kind)
        self.text = text
        self.xalign = xalign
        self.yalign = yalign
        assert state in {"normal","active"}, "bad state " + repr(state)
        if state == "normal":
            self.outlinecolour = cfg["buttonoutlinecolour"]
            self.bgcolour = cfg["buttonbgcolour"]
            self.textcolour = cfg["buttontextcolour"]
        elif kind == "normal":
            self.outlinecolour = cfg["buttonactiveoutlinecolour"]
            self.bgcolour = cfg["buttonactivebgcolour"]
            self.textcolour = cfg["buttonactivetextcolour"]
        else:
            self.outlinecolour = cfg["offdayactiveoutlinecolour"]
            self.bgcolour = cfg["offdayactivebgcolour"]
            self.textcolour = cfg["offdayactivetextcolour"]
        self.state = state
        self.borderinner = 7 # padding between outline and text
        self.borderouter = 5 # padding between screen edge and outline
    def totalheight(self):
        return ((self.borderouter + self.borderinner) * 2 +
                self.font.capsheight())
    def draw(self, group, w, h):
        textwidth = self.font.stringwidth(self.text)
        capheight = self.font.capsheight()
        bordertotal = self.borderouter + self.borderinner

        drawnwidth = textwidth + 2*self.borderinner
        spacingwidth = drawnwidth + 2*self.borderouter
        left = (w - spacingwidth) * self.xalign

        drawnheight = capheight + 2*self.borderinner
        spacingheight = drawnheight + 2*self.borderouter
        top = (h - spacingheight) * self.yalign

        svgrect(group, left + self.borderouter, top + self.borderouter,
                drawnwidth, drawnheight, {
                "fill": self.bgcolour,
                "stroke": self.outlinecolour,
                "stroke-width": svgdim(1.5),
                })

        self.font.draw(group,
                       left + bordertotal, top + bordertotal + capheight,
                       self.text, {"fill":self.textcolour})

class TimeDisplayFixed(object):
    def __init__(self, cfg):
        self.font = Font(cfg["timefont"], cfg["hhmmfontsize"])
        self.x = cfg["time_colon_x"]
        self.y = cfg["time_y"]
        self.colour = cfg["timecolour"]
    def draw(self, group, w, h):
        self.font.draw(group, self.x, self.y, ":", {"fill":self.colour})

class AlarmTimeDisplayFixed(object):
    def __init__(self, cfg):
        self.font = Font(cfg["messagefont"])
        self.textcolour = cfg["messagecolour"]
        self.pieces = ["ALARM ", None, None, ":", None, None, ":", None, None]
        self.y = cfg["message_bottom_y"] + self.font.capsheight()/2
        self.xpos = [0]
        self.digitwidth = max(self.font.stringwidth(d) for d in string.digits)
        for p in self.pieces:
            if p is None:
                w = self.digitwidth
            else:
                w = self.font.stringwidth(p)
            self.xpos.append(self.xpos[-1] + w)
    def digit_positions(self, w):
        xoff = (w - self.xpos[-1]) / 2
        return [x+xoff
                for x, p in zip(self.xpos, self.pieces)
                if p is None]
    def draw(self, group, w, h):
        xoff = (w - self.xpos[-1]) / 2
        for x, p in zip(self.xpos, self.pieces):
            if p is not None:
                self.font.draw(group, x+xoff, self.y, p,
                               {"fill":self.textcolour})

class Digit(object):
    def __init__(self, cfg, text, kind):
        if kind == "alarmtime":
            self.font = Font(cfg["messagefont"])
            self.x = cfg["alarm_digit_x"]
            self.y = cfg["message_bottom_y"] + self.font.capsheight()/2
            self.textcolour = cfg["messagecolour"]
        elif kind == "timehhmm":
            self.font = Font(cfg["timefont"], cfg["hhmmfontsize"])
            self.x = cfg["hhmm_digit_x"]
            self.y = cfg["time_y"]
            self.textcolour = cfg["timecolour"]
        elif kind == "timess":
            self.font = Font(cfg["timefont"], cfg["ssfontsize"])
            self.x = cfg["sec_digit_x"]
            self.y = cfg["time_y"]
            self.textcolour = cfg["timecolour"]
        else:
            assert False, "bad kind " + repr(kind)
        self.text = text
        self.colour = cfg["timecolour"]
    def draw(self, group, w, h):
        self.font.draw(group, self.x, self.y, self.text,
                       {"fill":self.textcolour})

class TimeAdjustArrow(object):
    def __init__(self, cfg, digitpos, direction, state):
        self.x = cfg["time_digit_mid_x"][digitpos]
        self.ybase = cfg["arrow_base_y"][direction]
        self.ytip = cfg["arrow_tip_y"][direction]
        self.halfwidth = cfg["arrow_halfwidth"]
        assert state in {"normal","active"}, "bad state " + repr(state)
        self.state = state
        if state == "normal":
            self.outlinecolour = cfg["buttonoutlinecolour"]
            self.bgcolour = cfg["buttonbgcolour"]
        else:
            self.outlinecolour = cfg["buttonactiveoutlinecolour"]
            self.bgcolour = cfg["buttonactivebgcolour"]
    def draw(self, group, w, h):
        svgpoly(group, [(self.x,self.ytip),
                        (self.x-self.halfwidth,self.ybase),
                        (self.x+self.halfwidth,self.ybase)], {
                "fill": self.bgcolour,
                "stroke": self.outlinecolour,
                "stroke-width": svgdim(1.5),
                })

class Message(object):
    def __init__(self, cfg, y, text, colour, fontscale=1.0):
        self.font = Font(cfg["messagefont"],
                         cfg["messagefont"]["size"] * fontscale)
        self.y = y + self.font.capsheight()/2
        self.text = text
        self.colour = colour
    def draw(self, group, w, h):
        self.font.draw(group, w/2, self.y, self.text,
                       {"fill":self.colour, "text-anchor":"middle"})

class DayOfWeek(Message):
    def __init__(self, cfg, text):
        Message.__init__(self, cfg, cfg["message_top_y"], text,
                         cfg["messagecolour"])
class ConfirmAlarm(Message):
    def __init__(self, cfg):
        Message.__init__(self, cfg, cfg["message_bottom_y"], "CONFIRM ALARM",
                         cfg["messagecolour"])
class NetworkFault(Message):
    def __init__(self, cfg):
        Message.__init__(self, cfg, cfg["message_top_y"], "NETWORK FAULT",
                         cfg["errormessagecolour"], fontscale=1.3)

def compute_layout(cfg, w, h):
    timefont = Font(cfg["timefont"], 1)
    secratio = cfg["secratio"]

    digitwidth = max(timefont.stringwidth(d) for d in string.digits)
    colonwidth = timefont.stringwidth(":")
    spacewidth = timefont.stringwidth(" ")

    timeborder = 5
    timewidth = 4 * digitwidth + colonwidth
    timewidth += secratio * (2 * digitwidth + spacewidth)

    scale = (w - 2*timeborder) / timewidth
    cfg["hhmmfontsize"] = scale
    cfg["ssfontsize"] = scale * secratio
    time_height = timefont.digitheight() * scale
    cfg["time_y"] = (h + time_height) / 2

    # Find the y-coordinates of messages and arrows.
    cfg["arrow_base_y"] = {}
    cfg["arrow_tip_y"] = {}
    dummy_button = Button(cfg, "WHATEVER", 0, 0, "normal", "normal")
    button_height = dummy_button.totalheight()
    y0, y1 = h/2 - time_height/2, button_height
    cfg["message_top_y"] = y0 + 0.5*(y1-y0)
    cfg["arrow_base_y"][+1] = y0 + 0.25*(y1-y0)
    cfg["arrow_tip_y"][+1] = y0 + 0.75*(y1-y0)
    y0, y1 = h/2 + time_height/2, h - button_height
    cfg["message_bottom_y"] = y0 + 0.5*(y1-y0)
    cfg["arrow_base_y"][-1] = y0 + 0.25*(y1-y0)
    cfg["arrow_tip_y"][-1] = y0 + 0.75*(y1-y0)

    cfg["arrow_halfwidth"] = scale * digitwidth * secratio * 0.4

    # Now compute the x-coordinates of time digits etc.
    #
    # This is a bit fragile because we instantiate an
    # AlarmTimeDisplayFixed below, which will complain if it doesn't
    # already know its y-coordinate. But there's no real reason why
    # the x computations _have_ to depend on the y-coordinate, so it
    # would be better to arrange that it could survive not knowing
    # that detail, and only fail if asked to actually draw something.

    consts = {}

    x = timeborder
    cfg["time_digit_mid_x"] = []

    cfg["hhmm_digit_x"] = xstart = int(round(x))
    consts["time_offsets"] = []
    for i in range(4):
        consts["time_offsets"].append(int(round(x)) - xstart)
        cfg["time_digit_mid_x"].append(x + digitwidth * scale/2)
        x += digitwidth * scale
        if i == 1:
            cfg["time_colon_x"] = int(round(x))
            x += colonwidth * scale

    x += spacewidth * secratio * scale

    cfg["sec_digit_x"] = xstart = int(round(x))
    consts["sec_offsets"] = []
    for i in range(2):
        consts["sec_offsets"].append(int(round(x)) - xstart)
        cfg["time_digit_mid_x"].append(x + digitwidth * secratio * scale/2)
        x += digitwidth * secratio * scale

    alarmdigitposns = AlarmTimeDisplayFixed(cfg).digit_positions(w)
    cfg["alarm_digit_x"] = int(round(alarmdigitposns[0]))
    consts["alarm_digit_offsets"] = [int(round(x - alarmdigitposns[0]))
                                     for x in alarmdigitposns]

    return consts

def make_sprite_array(cfg):
    sprites = []
    buttons = [
        ("SNOOZE", 0, 0, "normal"),
        ("SETUP", 0.5, 0, "normal"),
        ("SHUT UP", 1, 0, "normal"),
        ("ALARM ON", 0, 1, "normal"),
        ("CONFIRM", 0.5, 1, "normal"),
        ("ALARM OFF", 1, 1, "normal"),
        ("ALARM TIME", 0, 0, "normal"),
        ("BACK", 0.5, 0, "normal"),
        ("RESET TIME", 1, 0, "normal"),
        ("SNOOZE", 0, 1, "normal"),
        ("RE-DL", 0.5, 1, "normal"),
        ("OFF DAYS", 1, 1, "normal"),
        ("M", 1/14.0, 0.5, "offday"),
        ("T", 3/14.0, 0.5, "offday"),
        ("W", 5/14.0, 0.5, "offday"),
        ("T", 7/14.0, 0.5, "offday"),
        ("F", 9/14.0, 0.5, "offday"),
        ("S", 11/14.0, 0.5, "offday"),
        ("S", 13/14.0, 0.5, "offday"),
        ("M", 1/14.0, 1, "offday"),
        ("T", 3/14.0, 1, "offday"),
        ("W", 5/14.0, 1, "offday"),
        ("T", 7/14.0, 1, "offday"),
        ("F", 9/14.0, 1, "offday"),
        ("S", 11/14.0, 1, "offday"),
        ("S", 13/14.0, 1, "offday"),
    ]
    for name, x, y, kind in buttons:
        sprites.append(Button(cfg, name, x, y, kind, "normal"))
    for name, x, y, kind in buttons:
        sprites.append(Button(cfg, name, x, y, kind, "active"))
    sprites.append(TimeDisplayFixed(cfg))
    for d in string.digits:
        sprites.append(Digit(cfg, d, "timehhmm"))
    for d in string.digits:
        sprites.append(Digit(cfg, d, "timess"))
    for y in {+1,-1}:
        for x in range(6):
            sprites.append(TimeAdjustArrow(cfg, x, y, "normal"))
    for y in {+1,-1}:
        for x in range(6):
            sprites.append(TimeAdjustArrow(cfg, x, y, "active"))
    for day in ["MON", "TUES", "WEDNES", "THURS", "FRI", "SATUR", "SUN"]:
        sprites.append(DayOfWeek(cfg, day+"DAY"))
    sprites.append(AlarmTimeDisplayFixed(cfg))
    for d in string.digits:
        sprites.append(Digit(cfg, d, "alarmtime"))
    sprites.append(ConfirmAlarm(cfg))
    sprites.append(NetworkFault(cfg))

    assert(len(sprites) == 117) # edit Makefile if this changes
    return sprites

def main():
    parser = argparse.ArgumentParser(
        description='Generate graphics for Tring.')
    parser.add_argument(
        'sprite_index', nargs="?", type=lambda s:int(s,10), help=
        'Integer index of a sprite to generate (default: all).')
    parser.add_argument(
        '-c', '--config', help=
        'File to read local configuration options from.')
    parser.add_argument(
        '--constants', action='store_true', help=
        'Instead of generating any sprites, write out C arrays of x offsets'+
        ' between digit positions.')
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '--outdir', help=
        'Directory to write output SVGs to.')
    group.add_argument(
        '--stdout', action='store_true', help=
        'Write output SVG to standard output. (Probably pointless if more than one.)')
    args = parser.parse_args()

    cfg = setup_config(args)

    w, h = 320, 240
    consts = compute_layout(cfg, w, h)

    sprites = make_sprite_array(cfg)

    if args.constants:
        for var in sorted(consts):
            sys.stdout.write("const int %s[%d] = {%s};\n" % (
                    var, len(consts[var]),
                    ", ".join(["%d" % i for i in consts[var]])))
    else:
        # For historical reasons, sprites are indexed from 1.
        for index, sprite in enumerate(sprites, 1):
            if args.sprite_index is None or args.sprite_index == index:
                if args.stdout:
                    outfile = sys.stdout.buffer
                else:
                    outfile = "image%03d.svg" % index
                    if args.outdir is not None:
                        outfile = os.path.join(args.outdir, outfile)

                svg = SVG(w, h)
                sprite.draw(svg.group(), w, h)
                svg.save(outfile)

if __name__ == "__main__":
    main()
